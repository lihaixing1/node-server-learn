### nginx安装
brew install nginx
### nginx配置
- /usr/local/etc/nginx/nginx.conf
- nginx -t 检测是否正常
- nginx -c /etc/nginx/nginx.conf
- nginx 启动nginx
- nginx -s reload 重启nginx
- nginx -s stop 关闭nginx

$ systemctl status nginx.service

$ sudo pkill -f nginx
$ sudo systemctl start nginx