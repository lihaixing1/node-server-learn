const http = require('http');
const querystring = require('querystring');

/**
 * get 请求
 */
// const server = http.createServer((req, res) => {
//     res.writeHead(200, {'content-Type': 'text/html;charset=utf-8'});
//     console.log(req.url);
//     console.log(req.method);
//     const query = querystring.parse(req.url.split('?')[1])
//     res.end(`<pre>${JSON.stringify(query,null,' ')}</pre>`)
// });

/**
 * post 请求
 */
const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
        console.log('content-type', req.headers);
        let postData = '';
        req.on('data', (chunk) => {
            console.log(typeof chunk); // buffer数据
            postData += chunk.toString();
        });
        req.on('end', () => {
            // res.setHeader('content-Type', 'application/json');
            // 返回的永远是字符串
            res.end(JSON.stringify({
                url: req.url,
                method: req.method
            }));
        })
    } else {
        // res.writeHead(200, {'content-Type': 'text/html;charset=utf-8'});
        res.setHeader('content-Type', 'application/json');
        res.end(JSON.stringify({
            url: req.url,
            method: req.method
        }));
    }
});
server.listen(9000, () => {
    console.log('listening on port 9000 ~')
});

