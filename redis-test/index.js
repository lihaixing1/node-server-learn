const redis = require('redis');

// 创建客户端
const redisServer = redis.createClient(6379, '127.0.0.1');
redisServer.on('error', (err) => {
    console.error(err);
});

// 测试
redisServer.set('myName','haixing',redis.print);
// 这里是异步
redisServer.get('myName',(err,val)=>{
    if(err){
        console.error(err);
        return;
    }
    console.log('val:',val);
    redisServer.quit();
});