const queryString = require('querystring');
const handleBlogRouter = require('./src/router/blog');
const handleUserRouter = require('./src/router/user');
const {redisGet, redisSet} = require('./src/db/redis');
const {access, throwError} = require('./src/utils/log');
const {ErrorModel} = require('./src/model/resModel');
const getPostData = (req) => {
    const promise = new Promise((resolve, reject) => {
        if (req.method !== 'POST') {
            resolve({});
            return;
        }
        if (req.headers['content-type'] !== 'application/json') {
            resolve({});
            return;
        }
        let postData = '';
        req.on('error', (err) => {
            reject(err);
        });
        req.on('data', (chunk) => {
            postData += chunk.toString();
        });
        req.on('end', () => {
            if (!postData) {
                resolve({});
                return;
            }
            resolve(JSON.parse(postData))
        })
    });
    return promise;

};
const getCookieExpires = () => {
    const d = new Date();
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d.toGMTString();
};

// 手写session
// const SESSION_DATA = {};

const serverHandle = (req, res) => {
    access(`${req.method} -- ${req.url} -- ${req.headers['user-agent']} -- ${Date.now()}`);
    res.setHeader('content-type', 'application/json');

    const url = req.url;
    req.path = url.split('?')[0];
    // 解析query
    req.query = queryString.parse(url.split('?')[1]);

    // 解析cookie
    const cookie = req.headers.cookie || '';
    // todo 可能是数组，还需处理
    // req.cookie = queryString.parse(cookie.replace(/\s*;\s*/g,'&'));
    req.cookie = {};
    cookie.split(/\s*;\s*/g).forEach((item) => {
        if (!item) {
            return;
        }
        const arr = item.split('=');
        req.cookie[arr[0]] = arr[1];
    });

    // 解析session
    // let userId = req.cookie.userId;
    // let needSetCookie = false;
    // if (userId) {
    //     if (!SESSION_DATA[userId]) {
    //         SESSION_DATA[userId] = {};
    //     }
    // } else {
    //     needSetCookie = true;
    //     userId = `${Date.now()}_${Math.random()}`;
    //     SESSION_DATA[userId] = {};
    // }
    // req.session = SESSION_DATA[userId];

    // 解析session (使用redis)
    let userId = req.cookie.userId;
    let needSetCookie = false;
    if (!userId) {
        needSetCookie = true;
        userId = `${Date.now()}_${Math.random()}`;
        // SESSION_DATA[userId] = {};
        redisSet(userId, {});
    }
    req.sessionId = userId;
    redisGet(req.sessionId).then((sessionData) => {
        if (!sessionData) {
            redisSet(req.sessionId, {});
        }
        req.session = sessionData || {};
        return getPostData(req);
    }).then((postData) => {
        req.body = postData;

        const resultBlog = handleBlogRouter(req, res);
        const resultUser = handleUserRouter(req, res);
        if (resultBlog) {
            resultBlog.then(blogData => {
                if (needSetCookie) {
                    res.setHeader('set-cookie', `userId=${userId};path=/;httpOnly;expires=${getCookieExpires()}`);
                }
                res.end(JSON.stringify(blogData));
            }).catch((err) => {
                res.end(JSON.stringify(new ErrorModel('接口报错')));
                throwError(`${Date.now()} -- ${JSON.stringify(err)} -- ${req.url} -- ${JSON.stringify(req.body)}`);
            });
            return;
        }

        if (resultUser) {
            resultUser.then(userData => {
                if (needSetCookie) {
                    res.setHeader('set-cookie', `userId=${userId};path=/;httpOnly;expires=${getCookieExpires()}`);
                }
                res.end(JSON.stringify(userData));
            }).catch((err) => {
                res.end(JSON.stringify(new ErrorModel('接口报错')));
                throwError(`${Date.now()} -- ${JSON.stringify(err)} -- ${req.url} -- ${JSON.stringify(req.body)}`);
            });
            return;
        }

        res.writeHead(404, {"content-type": "text/plain"}); //纯文本
        res.write('404 not found\n');
        res.end();
    });

    // const resData = {
    //     name: 'lihaixing',
    //     age: 30,
    //     env: process.env.NODE_ENV
    // };
    // res.end(JSON.stringify(resData));
};
module.exports = serverHandle;