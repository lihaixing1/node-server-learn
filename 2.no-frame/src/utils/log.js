const fs = require('fs');
const path = require('path');

/**
 * 生成write流
 * @param fileName
 */
function createWriteStream(fileName) {
    const fullFileName = path.join(__dirname, '../../', 'logs/', fileName);
    const writeStream = fs.createWriteStream(fullFileName, {flags: 'a'});
    return writeStream;
}

/**
 * 写日志
 * @param writeStream
 * @param log
 */
function writeLog(writeStream, log) {
    writeStream.write(log + '\n'); //关键代码
}

/**
 * 写访问日志
 * @param log
 */
const accessWriteStream = createWriteStream('access.log');
const errorWriteStream = createWriteStream('error.log');

function access(log) {
    // 可以加判断 如果是开发环境，直接打印出来，如果是prd,就写日志
    writeLog(accessWriteStream, log);
}

function throwError(log) {
    // 可以加判断 如果是开发环境，直接打印出来，如果是prd,就写日志
    writeLog(errorWriteStream, log);
}

module.exports={
    access,
    throwError
};