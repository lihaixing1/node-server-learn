const {login} = require('../constroller/user');
const {SuccessModel, ErrorModel} = require('../model/resModel');
const {redisSet, redisGet} = require('../db/redis');

const handleUserRouter = (req, res) => {
    const method = req.method;

    if (method === 'POST' && req.path === '/api/user/login') {
        const {username, password} = req.body;
        const result = login(username, password);
        return result.then(user => {
            if (user) {
                // 设置cookie 全局有效 只允许后端来改 过期时间
                // res.setHeader('set-cookie', `username=${user.username};path=/;httpOnly;expires=${getCookieExpires()}`);
                // 设置session
                req.session.username = user.username;
                req.session.realname = user.realname;
                redisSet(req.sessionId, req.session);
                return new SuccessModel();
            }
            return new ErrorModel('登录失败');
        });
    }

    if (method === 'GET' && req.path === '/api/user/login-test') {
        if (req.session.username) {
            return Promise.resolve(new SuccessModel({
                username: req.session.username
            }));
        }
        return Promise.resolve(new ErrorModel('还未登录'));
    }
};

module.exports = handleUserRouter;
