const {
    getList,
    getDetail,
    newBlog,
    updateBlog,
    deleteBlog
} = require('../constroller/blog');
const {SuccessModel, ErrorModel} = require('../model/resModel');
// 统一的登录验证 更新、新建、删除都需要登录验证
const loginCheck = (req) => {
    if (!req.session.username) {
        return Promise.resolve(new ErrorModel('还未登录'));
    }
};
const handleBlogRouter = (req, res) => {
    const method = req.method;
    const id = req.query.id;
    if (method === 'GET' && req.path === '/api/blog/list') {
        let author = req.query.author || '';
        const keyword = req.query.keyword || '';
        if (req.query.isadmin) {
            if (loginCheck(req)) {
                return loginCheck(req)
            }
            author = req.session.username
        }

        const result = getList(author, keyword);
        return result.then(listData => {
            return new SuccessModel(listData);
        });
    }
    if (method === 'GET' && req.path === '/api/blog/detail') {
        const result = getDetail(id);
        return result.then(blog => {
            return new SuccessModel(blog);
        })
    }
    if (method === 'POST' && req.path === '/api/blog/new') {
        if (loginCheck(req)) {
            return loginCheck(req)
        }
        req.body.author = req.session.username;
        const postData = req.body;
        const result = newBlog(postData);
        return result.then((data => {
            return new SuccessModel(data);
        }));
    }
    if (method === 'POST' && req.path === '/api/blog/update') {
        if (loginCheck(req)) {
            return loginCheck(req)
        }
        const postData = req.body;
        const result = updateBlog(id, postData);
        return result.then(bool => {
            return bool ? new SuccessModel() : new ErrorModel('更新博客失败');
        });
    }
    if (method === 'POST' && req.path === '/api/blog/del') {
        if (loginCheck(req)) {
            return loginCheck(req)
        }
        const author = req.session.username;
        const result = deleteBlog(id, author);
        return result.then(bool => {
            return bool ? new SuccessModel() : new ErrorModel('删除博客失败');
        });
    }
};

module.exports = handleBlogRouter;