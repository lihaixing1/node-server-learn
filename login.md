### cookie
- cookie跨域不共享
- 每次发送请求都会将cookie一块发送的server
- server可以修改cookie并返回给浏览器
- 浏览器也可以通过js修改cookie(一般不这么做,有限制)

### cookie访问
- 淘宝页面访问淘宝server,将自己的cookie发到淘宝server就可以
- 淘宝页面访问百度server,将自己的cookie发给百度server,这是不可用的，也非常危险(将自己cookie泄露给其它网站)，应该发的是百度的cookie
### 浏览器中查看cookie
- response set-cookie
- request cookie

### 前端修改cookie
- document.cookie ='k1=100' 这是追加cookie, 而不是赋值
- document.cookie='k1=100;k2=200', 这种只能追加第一个，k2=200不会生效

### promise相关
- promise.resolve(data)直接返回一个promise

### cookie 缺陷
- cookie 会暴露username、手机号等信息，比较微信
- cookie 存储量有限（5k）,不宜存放大量信息
- 存放userId就可以，在server端有手机号或者username等(session)与之对应就可以了

### session 缺陷
- session信息存在运行内存中，当用户量过大时，会放不下
- nodejs程序是分成多个进程来跑，进程之间的session是不能共享的 (一个nodejs进程最多1.6G)
- 多核计算机可以同时处理多个进程
- 多机房多集群和多进程同理，都是session不能共享

### redis
- web server常用的缓存数据库, 数据放在内存中(读取速度快，同时也比较昂贵,断电就没了)
- 相比mysql，速度快（内存和硬盘不是一个数量级）
- redis和mysql都是工具，只不过存储方式不一样
- session访问比较频繁，因此要快，用redis
- 安装 brew install redis
- 可以解决多进程不能共享
##### 命令
- redis-server
- redis-cli
- set myName lihaixing  
 => ok
- get myName   
 => "lihaixing"
- keys *    
 => 1)"myName"
- del myName    
 => (integer) 1
 
 /etc/init.d/redis-server stop
 /etc/init.d/redis-server start
 /etc/init.d/redis-server restart
 ### cookie前端跨域不共享，因此要用ngix