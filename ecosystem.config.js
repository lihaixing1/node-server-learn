module.exports = {
    apps: [
        {
            name: 'blog-server',
            script: 'app.js',
            cwd: './4.blog-koa2',
            env: {
                COMMON_VARIABLE: true
            },
            env_production: {
                NODE_ENV: 'production'
            }
        },
        // {
        //     name: 'blog-html',
        //     script: 'index.html',
        //     cwd: './html-test',
        //     env: {
        //         COMMON_VARIABLE: true
        //     },
        //     env_production: {
        //         NODE_ENV: 'production'
        //     }
        // }
    ],
    deploy: {
        // "production" is the environment name
        production: {
            // SSH key path, default to $HOME/.ssh
            key: '~/.ssh/id_rsa',
            // SSH user
            user: 'blog_manager',
            // SSH host
            host: ['114.67.93.70'],
            port: '39999',
            // SSH options with no command-line flag, see 'man ssh'
            // can be either a single string or an array of strings
            ssh_options: ['StrictHostKeyChecking=no'],
            // ssh_options: ["StrictHostKeyChecking=no","PasswordAuthentication=no"],
            // GIT remote/branch
            ref: 'origin/master',
            // GIT remote
            repo: 'git@gitlab.com:lihaixing1/node-server-learn.git',
            // path in the server
            path: '/home/blog_manager/my-repository/production',
            env: {
                NODE_ENV: 'production'
            },
            // Pre-setup command or path to a script on your local machine
            //  'pre-setup': "/usr/bin/keychain ~/.ssh/blog_jd2-rsa; source ~/.keychain/$HOSTNAME-sh > /dev/null",
            'post-setup': 'echo $(pwd); cd 4.blog-koa2 ;npm install ;cd ..; pm2 start ecosystem.config.js',
            // eg: placing configurations in the shared dir etc
            // 'post-setup': "ls -la",
            // pre-deploy action
            // 'pre-deploy-local': "",
            // post-deploy action
            'post-deploy': 'echo $(pwd);cd 4.blog-koa2 ;npm install; cd ..; pm2 startOrRestart ecosystem.config.js'
        }
    }
};