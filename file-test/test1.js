const fs = require('fs');
const path = require('path');

const filename = path.resolve(__dirname, 'data.txt'); // 拼接目录，当前路径下的data.txt

// 文件操作是异步

// 读文件
// fs.readFile(filename,(err,data)=>{
//     if(err){
//         console.error(err);
//         return;
//     }
//     // data是二进制格式，需要转换成字符串  data数据可能很大，几个G, 这种方法不可靠，后面再说
//     console.log(data.toString())
// });

// 写入文件 内容太大也有问题，后面再说
// const opt = {
//     flag: 'a' // 表示追加写入 覆盖写入用 w
// };
//
// const content = 'hello, everyone\n';
// fs.writeFile(filename, content, opt, (err) => {
//     if (err) {
//         console.error(err);
//     }
// });

// 判断文件是否存在
fs.exists(filename,(exist)=>{
    console.log(exist);
});

