const fs = require('fs');
const path = require('path');

function getFileContent(fileName, callback) {
    const fullFileName = path.resolve(__dirname, 'files', fileName);
    fs.readFile(fullFileName, (err, data) => {
        if (err) {
            console.error(err);
            return;
        }
        // console.log(typeof data); // object
        // console.log(typeof data.toString()); // string
        callback(JSON.parse(data.toString()));
    });
}

//
// getFileContent('a.json', (adata) => {
//     console.log(adata);
//     getFileContent(adata.next, (bdata) => {
//         console.log(bdata);
//         getFileContent(bdata.next, (cdata) => {
//             console.log(cdata);
//         });
//     });
// });

function getFileContentPromise(fileName) {
    const promise = new Promise((resolve, reject) => {
        const fullFileName = path.resolve(__dirname, 'files', fileName);
        fs.readFile(fullFileName, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(JSON.parse(data.toString()));
        });
    });
    return promise;
}

getFileContentPromise('a.json').then((adata) => {
    console.log(adata);
    return getFileContentPromise(adata.next);
}).then((bdata) => {
    console.log(bdata);
    return getFileContentPromise(bdata.next);
}).then((cdata) => {
    console.log(cdata);
    return cdata;
}).then((cdata)=>{
    console.log(cdata);
});

async function readFileData() { // 返回的也是promise

    // 可以通过try catch截获reject数据
    const aData =await getFileContentPromise('a.json');  // await后面跟promise对象
    console.log('adata',aData);
    const bData =await getFileContentPromise(aData.next);
    console.log('bdata',bData);
    const cData =await getFileContentPromise(bData.next);
    console.log('cdata',cData);

    return cData;
}

// 错误函数可以卸载错误回掉里，也可以卸载catch()里，两个都写了，只执行一个