### sql语句
````
use myblog
show tables

insert into users (username,`password`,realname) values ('lisi','123456','李四');
select * from users
select id,username from users;
select * from users where id=1 and `password`=123456
select * from users where id=1 or id=2
select * from users where username like '%zhang%'
select * from users order by id
select * from users order by id desc
````
````
SET SQL_SAFE_UPDATES=0; 解决update安全问题
select * from users
update users set realname='李四2' where username='lisi';
insert into users (username,`password`,realname) values ('lisi','123456','李四');
delete from users where username = 'lisi';
-- 软删除
update users set state=0 where username='lisi'
-- 不等于  
select * from users where state<>0
````
### 执行完node sql语句的返回参数
- insert 返回参数
````
OkPacket {
  fieldCount: 0,
  affectedRows: 1,
  insertId: 3,  // 有用
  serverStatus: 2,
  warningCount: 0,
  message: '',
  protocol41: true,
  changedRows: 0 
}
````
- update 返回参数
````
OkPacket {
  fieldCount: 0,
  affectedRows: 1, // 有用
  insertId: 0,
  serverStatus: 34,
  warningCount: 0,
  message: '(Rows matched: 1  Changed: 1  Warnings: 0',
  protocol41: true,
  changedRows: 1 // 有用
}
````
- delete 返回参数
````
OkPacket {
  fieldCount: 0,
  affectedRows: 1, // 有用
  insertId: 0,
  serverStatus: 34,
  warningCount: 0,
  message: '',
  protocol41: true,
  changedRows: 0  // 有用
}

````