/**
 * 栗子1
 */
// 直接把输入导入到输出
// process.stdin.pipe(process.stdout);

/**
 * 栗子2
 */
// const http = require('http');
// const server = http.createServer((req, res) => {
//     if (req.method === 'POST') {
//         req.pipe(res); // 流的一种关系
//     }
// });
// server.listen(8000);

/**
 * 栗子3
 */
// const fs = require('fs');
// const path = require('path');
// const filename1 = path.resolve(__dirname, 'data.txt');
// const filename2 = path.resolve(__dirname, 'data-bak.txt');
//
// const readStream = fs.createReadStream(filename1);
// const writeStream = fs.createWriteStream(filename2);
// readStream.pipe(writeStream);
//
// readStream.on('data', (chunk) => {
//     console.log(chunk.toString());
// });
// readStream.on('end', () => {
//     console.log('copy done');
// });



/**
 * 栗子4
 */
const fs = require('fs');
const path = require('path');
const filename1 = path.resolve(__dirname, 'data.txt');

//
// const http = require('http');
// const server = http.createServer((req, res) => {
//     if (req.method === 'GET') {
//         const readStream = fs.createReadStream(filename1);
//         readStream.pipe(res); // 流的一种关系
//     }
// });
// server.listen(8000);
// const a =path.resolve(__dirname,'/../stream-test/test1.js');
// const b =path.join(__dirname,'..','/stream-test/test1.js');
// console.log(a,b)