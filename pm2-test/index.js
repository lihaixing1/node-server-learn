const http = require('http');


/**
 * post 请求
 */
const server = http.createServer((req, res) => {
    // console.log('模拟access','access.log');
    // console.error('模拟error','error.log');
    if(req.url==='/error'){
        throw new Error('出错了a');
    }
    res.setHeader('content-Type', 'application/json');
    res.end(JSON.stringify({
        url: req.url,
        method: req.method
    }));
});
server.listen(9000, () => {
    console.log('listening on port 9000 ~')
});

