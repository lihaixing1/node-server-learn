### 拆分日志
- crontab命令 定时任务 用来拆分日志
- 格式： ***** command 分钟 小时 日期 月份 星期 执行命令(shell脚本) 
- 过程： 将acess.log拷贝并重命名为2019-02-10.access.log；
        清空access.log文件 ， 继续积累日志
- shell脚本是在系统中执行，不是在nodejs中执行，效率更快

### shell用法
> sh copy.sh

### crontab用法
> crontab -e 进入编辑模式
````
0 * * * * sh /Users/lihaixing/Documents/learningDir/node_server_learn/2.no-frame/src/utils/copy.sh
````
> crontab -l 查看有哪些任务
> chmod u+s /usr/bin/crontab 赋予权限

### 日志分享 readline

### 安全
> sql注入攻击
- select * from users where username = 'zhangsan' --' and password=1234
- select * from users where username = 'zhangsan';delete from users --' and password=1234
- 用escape以防sql注入
- select username,realname from users where username='zhangsan\' --' and password='123456'
> xss攻击 参杂恶意js代码获取信息
- 措施：把js代码转换成普通字符 xss包 npm i xss --save
- 栗子：\<script\>alert(document.cookie)\</script\>
> 密码加密