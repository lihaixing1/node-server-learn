### 安装pm2
- npm install pm2 -g
- pm2 --version
### 使用
- pm2 start
- pm2 list 进程列表
- pm2 restart <appname>/<id> 重启
- pm2 stop/delete <appname>/<id> 停止/删除
- pm2 info <appname>/<id> 查看信息
- pm2 log <appname>/<id> 日志
- pm2 monit <appname>/<id> 监控信息

### 优点
- node nodemon 进程奔溃不能自动重启
- pm2 进程奔溃/出错后会重启

### 配置文件