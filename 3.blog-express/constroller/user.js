const {exec,escape} = require('../db/mysql');
const {genPassword} = require('../utils/cryp');
const login = (username,password) => {
    // 用escape以防sql注入
    const sql =`select username,realname from users where username=${escape(username)} and password=${escape(genPassword(password))}`;
    return exec(sql).then(users=>{
        return users[0];
    });
};
module.exports = {
    login
};