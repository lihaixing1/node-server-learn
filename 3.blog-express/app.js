var createError = require('http-errors'); // httpServer 只不过更有好得处理了404
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const fs = require('fs');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const blogRouter = require('./routes/blog');
const userRouter = require('./routes/user');

const redisServer = require('./db/redis');
const sessionStore = new RedisStore({
    client: redisServer
});

var app = express(); //本次app请求的实例
const env = process.env.NODE_ENV;

// view engine setup   视图暂时不管 前端页面
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

if (env !== 'production') {
    app.use(logger('dev', {
        stream: process.stdout // 默认的 可以不加 输出到控制台
    })); // 可以自动生成日志 当然还需要一些配置，后面会说
} else {
    const fullFileName = path.join(__dirname, 'logs', 'access.log');
    const writeStream = fs.createWriteStream(fullFileName, {flags: 'a'});
    app.use(logger('combined', {  // 一般线上用到combined
        stream: writeStream // 默认的 可以不加
    })); // 可以自动生成日志 当然还需要一些配置，后面会说
}

app.use(express.json()); // 相当于noframe中的getPostData, 将post数据转成json对象
app.use(express.urlencoded({extended: false})); // 将content-type不是application/json得post数据转成json对象
app.use(cookieParser()); // 解析cookie 后续就能通过req.cookies访问
// app.use(express.static(path.join(__dirname, 'public')));  // 不需要，前端得内容
// 处理session
app.use(session({
    secret: 'Waznb_0909',
    cookie: {
        path: '/', // 默认 可不写
        httpOnly: true, // 默认
        maxAge: 24 * 60 * 60 * 1000
    },
    store: sessionStore
}));
// app.use('/', indexRouter); // 父路径 会和子路径共同拼接成路由
// app.use('/users', usersRouter); // 父路径
app.use('/api/blog', blogRouter); // 父路径
app.use('/api/user', userRouter); // 父路径

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
