var express = require('express');
var router = express.Router();
const loginCheck = require('../middleware/loginCheck');
const {
    getList,
    getDetail,
    newBlog,
    updateBlog,
    deleteBlog
} = require('../constroller/blog');
const {SuccessModel, ErrorModel} = require('../model/resModel');

/* GET home page. */
router.get('/list', function (req, res, next) {   // 这里的回调函数就是匹配各种路径、方式
                                                  // json 相当于 JSON.stringify  顺便还设置了header application/json
    let author = req.query.author || '';
    const keyword = req.query.keyword || '';
    if (req.query.isadmin) {
        if (!req.session.username) {
            res.json(new ErrorModel('还未登录'));
            return;
        }
        author = req.session.username
    }

    const result = getList(author, keyword);
    result.then(listData => {
        res.json(new SuccessModel(listData));
    });
});

router.get('/detail', function (req, res, next) {
    // json 相当于 JSON.stringify  顺便还设置了header application/json
    const result = getDetail(req.query.id);
    result.then(blog => {
        res.json(new SuccessModel(blog));
    })
});

router.post('/new', loginCheck, function (req, res, next) {
    req.body.author = req.session.username;
    const postData = req.body;
    const result = newBlog(postData);
    result.then((data => {
        res.json(new SuccessModel(data));
    }));
});

router.post('/update', loginCheck, function (req, res, next) {
    const postData = req.body;
    const result = updateBlog(req.query.id, postData);
    result.then(bool => {
        res.json(bool ? new SuccessModel() : new ErrorModel('更新博客失败'));
    });
});

router.post('/del', loginCheck, function (req, res, next) {
    const author = req.session.username;
    const result = deleteBlog(req.query.id, author);
    result.then(bool => {
        res.json(bool ? new SuccessModel() : new ErrorModel('删除博客失败'));
    });
});

module.exports = router;