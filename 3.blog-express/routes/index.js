var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {   // 这里的回调函数就是匹配各种路径、方式
  res.render('index', { title: 'Express' }); // 模板 不用管
});

module.exports = router;
