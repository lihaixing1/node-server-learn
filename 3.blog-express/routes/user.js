var express = require('express');
var router = express.Router();
const {login} = require('../constroller/user');
const {SuccessModel, ErrorModel} = require('../model/resModel');

router.post('/login', function (req, res, next) {   // 这里的回调函数就是匹配各种路径、方式
                                                    // json 相当于 JSON.stringify  顺便还设置了header application/json
    const {username, password} = req.body;
    const result = login(username, password);
    result.then(user => {
        if (user) {
            // 设置cookie 全局有效 只允许后端来改 过期时间
            // res.setHeader('set-cookie', `username=${user.username};path=/;httpOnly;expires=${getCookieExpires()}`);
            // 设置session
            req.session.username = user.username;
            req.session.realname = user.realname;
            res.json(new SuccessModel());
            return;
        }
        res.json(new ErrorModel('登录失败'));
    });
});

router.get('/login-test', (req, res, next) => {
    const session = req.session;
    if (session.username) {
        res.json({
            errno: 0,
            msg: '测试成功'
        });
        return;
    }
    res.json({
        errno: -1,
        msg: '测试失败'
    });
});

router.get('/session-test', (req, res, next) => {
    const session = req.session;
    if (session.viewNum === null) {
        session.viewNum = 0;
    }
    session.viewNum++;
    res.json({
        viewNum: session.viewNum
    });
});

module.exports = router;