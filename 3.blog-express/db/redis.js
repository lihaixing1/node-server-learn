const redis = require('redis');
const {REDIS_CONF} = require('../conf/db');

const redisServer = redis.createClient(REDIS_CONF.port, REDIS_CONF.host);
redisServer.on('error', (err) => {
    console.error(err);
});


// function redisGet(key) {
//     const promise = new Promise((resolve, reject) => {
//         redisServer.get(key, (err, val) => {
//             if (err) {
//                 reject(err);
//                 return;
//             }
//             // 没有这个key就是null
//             if (val === null) {
//                 resolve(null);
//                 return;
//             }
//             try {
//                 resolve(JSON.parse(val))
//             } catch (ex) {
//                 resolve(val);
//             }
//         });
//     });
//     return promise;
// }
//
// function redisSet(key, val) {
//     if (typeof val === 'object') {
//         val = JSON.stringify(val);
//     }
//     redisServer.set(key, val, redis.print);
// }

module.exports = redisServer;
