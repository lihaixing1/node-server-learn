const router = require('koa-router')()
const {login} = require('../constroller/user');
const {SuccessModel, ErrorModel} = require('../model/resModel');
router.prefix('/api/user');
router.post('/login', async function (ctx, next) {   // 这里的回调函数就是匹配各种路径、方式
                                                     // json 相当于 JSON.stringify  顺便还设置了header application/json
    const {username, password} = ctx.request.body;
    const user = await login(username, password);
    if (user) {
        // 设置cookie 全局有效 只允许后端来改 过期时间
        // res.setHeader('set-cookie', `username=${user.username};path=/;httpOnly;expires=${getCookieExpires()}`);
        // 设置session
        ctx.session.username = user.username;
        ctx.session.realname = user.realname;
        ctx.body = new SuccessModel();
        return;
    }
    ctx.body = new ErrorModel('登录失败');
});

router.get('/login-test', async (ctx, next) => {
    const session = ctx.session;
    if (session.username) {
        ctx.body = {
            errno: 0,
            msg: '测试成功'
        };
        return;
    }
    ctx.body = {
        errno: -1,
        msg: '测试失败'
    };
});

// router.get('/session-test', (req, res, next) => {
//     const session = req.session;
//     if (session.viewNum === null) {
//         session.viewNum = 0;
//     }
//     session.viewNum++;
//     res.json({
//         viewNum: session.viewNum
//     });
// });

module.exports = router;