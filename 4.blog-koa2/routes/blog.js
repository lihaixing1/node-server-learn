const router = require('koa-router')()

const loginCheck = require('../middleware/loginCheck');

router.prefix('/api/blog')
const {
    getList,
    getDetail,
    newBlog,
    updateBlog,
    deleteBlog
} = require('../constroller/blog');
const {SuccessModel, ErrorModel} = require('../model/resModel');

/* GET home page. */
router.get('/list', async function (ctx, next) {   // 这里的回调函数就是匹配各种路径、方式
                                                  // json 相当于 JSON.stringify  顺便还设置了header application/json
    let author = ctx.query.author || '';
    const keyword = ctx.query.keyword || '';
    if (ctx.query.isadmin) {
        if (!ctx.session.username) {
            ctx.body = new ErrorModel('还未登录');
            return;
        }
        author = ctx.session.username
    }

    const result = await getList(author, keyword);
    ctx.body= new SuccessModel(result);
});

router.get('/detail', async function (ctx, next) {
    const result = await getDetail(ctx.query.id);
    ctx.body= new SuccessModel(result);
});

router.post('/new', loginCheck, async function (ctx, next) {
    ctx.request.body.author = ctx.session.username;
    const postData = ctx.request.body;
    const result =await newBlog(postData);
    ctx.body= new SuccessModel(result);
});

router.post('/update', loginCheck, async function (ctx, next) {
    const postData = ctx.request.body;
    const bool = await updateBlog(ctx.query.id, postData);
    ctx.body= bool ? new SuccessModel() : new ErrorModel('更新博客失败');
});

router.post('/del', loginCheck, async function (ctx, next) {
    const author = ctx.session.username;
    const bool = await deleteBlog(ctx.query.id, author);
    ctx.body= bool ? new SuccessModel() : new ErrorModel('删除博客失败');
});

module.exports = router;