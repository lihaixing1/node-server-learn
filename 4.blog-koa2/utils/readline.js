const fs = require('fs');
const path = require('path');
const readline = require('readline');

const filename = path.join(__dirname, '../../', 'logs/', 'access.log');
const readStream = fs.createReadStream(filename);
const rl = readline.createInterface({
    input: readStream
});
let sum = 0;
let chromeNum = 0;
rl.on('line', (lineData) => {
    if (!lineData) {
        return;
    }
    const arr = lineData.split('--');
    if (arr[2] && arr[2].indexOf('Chrome') > -1) {
        chromeNum++;
    }
    sum++;
});
rl.on('close', () => {
    console.log('chrome 占比：', chromeNum / sum);
});