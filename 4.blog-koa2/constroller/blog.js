const {exec} = require('../db/mysql');
const xss = require('xss');
const getList = async (author, keyword) => {
    let sql = `select * from blogs where 1=1 `;
    if (author) {
        sql += `and author='${author}'`
    }
    if (keyword) {
        sql += `and title like '%${keyword}%' `
    }
    sql += `order by createTime desc;`;
    return await exec(sql);
};
const getDetail = async (id) => {
    let sql = `select * from blogs where id=${id}`;
    const rows = await exec(sql);
    return rows[0];
};
const newBlog = async (postData) => {
    const title = postData.title;
    const content = postData.content;
    const author = postData.author;
    const createTime = Date.now();
    const sql = `
        insert into blogs (title,content,createTime,author) values ('${xss(title)}','${xss(content)}','${createTime}','${author}');
    `;
    const insertData = await exec(sql);
    return {
        id: insertData.insertId
    }
};
const updateBlog = async (id, postData = {}) => {
    const title = postData.title;
    const content = postData.content;
    const sql = `
        update blogs set title='${title}',content='${content}' where id=${id};
    `;
    const updata = await exec(sql);
    return updata.affectedRows > 0;
};
const deleteBlog = async (id, author) => {
    const sql = `
        delete from blogs where id=${id} and author='${author}';
    `;
    const updata = await exec(sql);
    return updata.affectedRows > 0;
};
module.exports = {
    getList,
    getDetail,
    newBlog,
    updateBlog,
    deleteBlog
};