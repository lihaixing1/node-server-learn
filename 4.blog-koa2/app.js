const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
const morgan = require('koa-morgan');
const session = require('koa-generic-session');
const redisStore = require('koa-redis');
const path = require('path');
const fs = require('fs');
const {REDIS_CONF} = require('./conf/db');

const env = process.env.NODE_ENV;
if (env !== 'production') {
    app.use(morgan('dev', {
        stream: process.stdout // 默认的 可以不加 输出到控制台
    })); // 可以自动生成日志 当然还需要一些配置，后面会说
} else {
    const fullFileName = path.join(__dirname, 'logs', 'access.log');
    const writeStream = fs.createWriteStream(fullFileName, {flags: 'a'});
    app.use(morgan('combined', {  // 一般线上用到combined
        stream: writeStream // 默认的 可以不加
    })); // 可以自动生成日志 当然还需要一些配置，后面会说
}

// 处理session
app.keys = ['Waznb_0909'];
app.use(session({
    cookie: {
        path: '/', // 默认 可不写
        httpOnly: true, // 默认
        maxAge: 24 * 60 * 60 * 1000
    },
    store: redisStore({
        all: `${REDIS_CONF.host}:${REDIS_CONF.port}`
    })
}));


const index = require('./routes/index')
const users = require('./routes/users')
const blog = require('./routes/blog')
const user = require('./routes/user')

// error handler
onerror(app)

// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}))
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))

app.use(views(__dirname + '/views', {
    extension: 'pug'
}))

// logger
app.use(async (ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

// routes
app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())
app.use(blog.routes(), blog.allowedMethods())
app.use(user.routes(), user.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
    console.error('server error', err, ctx)
});

module.exports = app
