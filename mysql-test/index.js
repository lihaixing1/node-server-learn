const mysql = require('mysql');
/**
 * 创建链接对象
 * @type {Connection}
 */
const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'lhx880929',
    port: 3306,
    database: 'myblog'
});

// 开始链接
con.connect();

// 执行sql语句 带回掉的
const sql1 = 'select * from users;';
const sql2 = `insert into users (username,\`password\`,realname) values ('wangwu','123456','王武');`;
const sql3 = `update users set realname='李四2' where username='lisi';`;
const sql4 = `delete from users where username = 'wangwu'`;
con.query(sql4, (err, result) => {
    if (err) {
        console.error(err);
        return;
    }
    console.log(result);
});

// 关闭链接
con.end();