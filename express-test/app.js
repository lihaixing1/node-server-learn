// const express = require('express');
const express = require('./self-express');

// 本次http请求实例
const app = express();

app.use((req, res, next) => {
    console.log('请求开始...', req.method, req.url);
    next();
});


app.use((req, res, next) => {
    // 假设处理cookie
    req.cookie = {
        userId: 'abc123'
    };
    next();
});

app.use((req, res, next) => {
    // 假设处理post data
    // 异步
    setTimeout(() => {
        req.body = {
            a: 100,
            b: 200
        };
        console.log('处理postData')
        next();
    });
});

// 用use 不过get还是post都会触发
app.use('/api', (req, res, next) => {
    console.log('处理 api路由');
    next();
});

app.get('/api', (req, res, next) => {
    console.log('get api路由');
    next();
});

app.post('/api', (req, res, next) => {
    console.log('get api路由');
    next();
});
// 这样写多个函数，意思是一样的 ,一般用于加验证
app.get('/api/get-cookie',(req, res, next) => {
    console.log(5);
    next();
}, (req, res, next) => {
    console.log('get cookie');
    res.json({
        errno: 0,
        data: req.cookie
    });
});

app.post('/api/get-post-data', (req, res, next) => {
    console.log('post get post data');
    res.json({
        errno: 0,
        data: req.body
    });
});

app.use((req, res, next) => {
    console.log('处理404');
    res.json({
        errno: -1,
        data: '404 not found'
    });
    // next();
});

app.listen(4000, () => {
    console.log('server is running on port 4000');
});

